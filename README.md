# Programming Notes

> These notes are for my own personal use, any educational value is purely accidental.


Online version available here: [![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://muskatel.gitlab.io/programming-notes/)

Current build: [![pipeline status](https://gitlab.com/muskatel/programming-notes/badges/main/pipeline.svg)](https://gitlab.com/muskatel/programming-notes/-/commits/main)


## Maintainers

Craig Marais @muskatel

Greg Linklater @EternalDeiwos

## License

MIT License, see license file.

---

Copyright 2022, Craig Marais (@muskatel)
