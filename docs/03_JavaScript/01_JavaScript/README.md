# JavaScript

- Why use JS on the server?
- Technical differences (what do we lose? what do we gain?)
- How to setup Node
- How to execute arbitrary JavaScript code manually
- Using modules (local)
- Using Filesystem, OS, and http