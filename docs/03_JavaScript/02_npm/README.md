# npm

- Understanding packages and dependencies
- Creating `Package.json`
- Setting up `.gitignore` for NPM projects
- Exploring and installing packages
- Using `nodemon`