# Databases with Node
- SQL basics
- Using SQLite3 for local data
- Other server side options
	- MongoDB
	- PostgreSQL
	- MySQL
- SQL cloud solutions