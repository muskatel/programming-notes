# Working with Git

## Learning Objectives
After completion a learner will be able to ...
- **carry out** the installation of git and it's configuration
- **state** the purpose and use of the various git key words (clone, pull, commit, push, and branch)
- **explain** the purpose of the .gitignore file
- **implement** an appropriate .gitignore file through online/external resources

Git is a distributed version control software, allowing you to collaborate with others while maintaining _snapshots_ of your code at defined points. Git is often used in an online (distributed) fashion, but it can also be used solely locally on your device.

As a tool for collaborative software development, Git is a critical part of your future as a developer!

## Installation and Setup

In all instances, the installation of Git is relatively quick and easy.

On Windows:

Install `Git for Windows` available [here](https://gitforwindows.org/)

On Macintosh (You will need to install [Homebrew](https://brew.sh/) first):

```bash
$ brew install git
```

On Linux (or WSL) use your preferred package manager:

```bash
$ sudo apt install git
```

### Checking the version

Once installed, you can check the version by running the following command:

```bash
$ git --version
git version 2.33.0
```

From this point, you could start using Git in an unsecured way, but for this course, we strongly advocate using SSH at all times.

### Ubuntu and the latest version

Some of the command we use later will expect a _newer_ version of Git, and Ubuntu 20.04 does not have the latest version and will not update to it under normal circumstances.

Version check reports a version less than `2.28.0`:
```bash
$ git --version
git version 2.25.1
```

We need to _encourage_ a newer version here:

Start by adding the following PPA for the latest stable version of Git:

```bash
$ sudo add-apt-repository ppa:git-core/ppa
```

Then perform an update of Git:

```bash
$ sudo apt-get update
$ sudo apt install git
```
 Now the version reported should be inline with `2.33.0` or newer.

## SSH-Keys

### Setting up keys

#### Open a terminal

**Windows:**

Use git bash:

- Open `Explorer`
- Right-Click inside any folder
- Select `Open Git Bash here`

> If you encounter permission errors later you may need to run Git Bash at an administrative level

**Mac or Linux:**

Any terminal will do!

#### Check if you already have a key

> If you already have a key **DO NOT** generate a new one!
>
> In that case, skip ahead to `Copying the key to the clipboard`

Run the following command from the terminal we just opened:

```bash
$ ls -al ~/.ssh
```

> These commands will not work from the Windows Command Prompt, they _may_ work from the Windows Powershell, but for consistency, we recommend using Git Bash.

The command above will display a list of files.
If any exist, they will be in pairs (one with no extension and another with a `.pub` extension).

This image shows my keys (and `known_hosts` file) on my MacBook:

![My Keys](../resources/02_keys.png)

> These will either be `id_rsa` or `id_ed25519`
>
> The second half denotes the encryption used, `ed25519` is now recommended instead of `rsa` because of security concerns.

If any file exists then you already have a key(s)!

**The Public Key**

This file ends with `.pub` extension (`id_ed25519.pub`), and can be shared freely. It is used by others to verify your identity.

> Think of it as your photo ID, when somebody sees it they can confirm that you are who you say you are.

**The Private Key**

This file ends no extension (`id_ed25519`)

This is the private key, and should **NEVER** be copied or shared!

> If the public key is photo ID, then the private key would be your physical face.

![Never share private keys](../resources/03_sharekeys.jpg)

#### Generating a key

> **Are you sure?**
>
> This process is non-reversible, and if you’ve used a key somewhere else you will need to update that one again with any newly generated key.

The following steps will generate a new key using `ed25519`, but read through all the steps before proceeding as there are several prompts that appear.

1. Start with the following command, using your e-mail instead of `you@example.com`:

```bash
$ ssh-keygen -t ed25519 -C "you@example.com"
```

2. Let the key be created in the default location:

```
/something/something/.ssh/id_ed25519
```

You will need to press `Enter`

3. You will be prompted to enter a passphrase (password) and repeat it after.
   > The prompt **will not** show characters (or `*`'s as you type)
4. When done, you will be presented a visual representation of your key.
5. Check if you have a key agent running:

```bash
$ eval $(ssh-agent -s)
```

You should be presented with a `pid` (process ID) and a number, this indicates that the process is running.

> The `ssh-agent -s` command will start the agent if it is not running.

6. Add your key to the agent:

```bash
$ ssh-add ~/.ssh/id_ed25519
```

At this point, you may be asked to enter the passphrase for the key.

The key is now generated and ready to be used to identify you, but you will need a separate key for each device (computer) that you want to be identified from.

Using the same e-mail address, it is possible to have multiple devices _registered_ as your identity, such as a desktop and a laptop. These different devices have unique keys that can have separate passphrases.

#### Copying the key to the clipboard

To use the key (such as on Github/Gitlab) you will need to copy it out of the `id_ed25519.pub` file, this is easy to achieve with one of the following commands:

On Windows:

```bash
$ clip < ~/.ssh/id_ed25519.pub
```

On Windows Subsystem for Linux

```bash
$ clip.exe < ~/.ssh/id_ed25519.pub
```

On Macintosh:

```bash
$ pbcopy < ~/.ssh/id_ed25519.pub
```

On Linux (install `xclip` first):

```bash
$ sudo apt install xclip
$ xclip -sel clip < ~/.ssh/id_ed25519.pub
```

> Within you account settings on Github/Gitlab you will need to paste the key in and give it an identifying name, it's a good idea to match this name to the computer itself.

#### Global settings

While the SSH key is very secure and has our e-mail address stored in it, there are still two settings we can set within the Git application to help it better _reflect_ our identity.

We start by setting our `global user name`, as follows:

```
$ git config --global user.name "your name"
```

This ensures that our git messages will have our name attached to them by default.

Secondly, we explicitly specify our e-mail address as the one we set in our key:

```
$ git config --global user.email you@example.com
```

These two steps may seem trivial, but in the long run, they will help us work easier with Git.

##### Default Branch

For many years the default was `master` and many legacy systems will still expect the default branch to be labelled `master`, however in recent years the major providers of free Git hosting ([GitHub](https://GitHub.com) and [GitLab](https://GitLab.com)) have chosen to switch the default to `main`.

To avoid compatibility issues and headaches I set my default branch for new repositories to `main` using the following command:

```bash
git config --global init.defaultBranch main
```

> This option does not work unless you updated you version of git to version 2.28+

You are perfectly free to set your default branch as you choose and but the remainder of these notes will assume the use of `main` as the default branch for a given repository, and to avoid future problems I suggest that you do the same.

> Be aware that many legacy repositories will still use the old default of `master` and you should not automatically assume that they have been changed to `main`.

##### Other Defaults

Similarly, we suggest that you run the following three commands to configure some other default behaviours:

```bash
$ git config --global pull.rebase true
$ git config --global fetch.prune true
$ git config --global diff.colorMoved zebra
```

Explanations of theses can be found [here](https://spin.atomicobject.com/2020/05/05/git-configurations-default/).

The above changes are saved to the `~/.gitconfig` file, and when done your configuration file should look something like this:

```conf
[user]
  name = Craig Marais
  email = craigm@meerkatio.net
[init]
  defaultBranch = main
[fetch]
  prune = true
[pull]
  rebase = true
[diff]
  colorMoved = zebra
```

> Use a command such as `$ code ~/.gitconfig` or `$ nano ~/.gitconfig` to open it quickly.

## Entering passwords gets annoying

You can minimize the number of times that you need to enter a password by creating the `~/.ssh/config` file and adding the contents shown below.

The "`UseKeychain yes`" is commented out because it is only used on macOS.

**~/.ssh/config**
```
Host *
  AddKeysToAgent yes
#  UseKeychain yes
  IdentityFile ~/.ssh/id_ed25519
```

> Reload your shell after saving changes to this file

The last line (`IdentityFile ~/.ssh/id_ed25519`) is optional, but it shows that we can specify different keys for different hosts should we wish.

## Cloning

Consider the use of VSCode, suppose we find a bug while using it. We should start by checking the official repository ([Visual Studio Code - Open Source](https://github.com/microsoft/vscode)) for the `Issue`. It is completely plausible that someone else has encountered the same bug as us, and it may even be marked for correction in the next release!

If we do not find the bug then it is good practise to take the time to report it correctly, often open source projects will have templates for submitting bug/issues. Check the `Contributing` section of the `README.md`.

We could as developers also download a copy of the source code and try to find the problem in the code ourselves. Looking at the repository, we can see a green `Code` button. By pressing this button we will (depending on our account) have a few options:

- Download (Download as ZIP)
- Clone with HTTPS
- Clone with SSH

> On GitLab there is instead a blue `Clone` button.

Once we have the source code we _could_ build the project ourselves, and to fix any bugs we will need to do so. It is beyond the scope of this course to help you setup the toolchain needed for every possible open source project, but it

### Download (Download as ZIP)

This lets us use our browser to download the current branch of the repository as a compressed archive, this is useful when we just want to see how some piece of code was executed or when we will not be contributing back to the repository.

If we want to get an updated version of a repository that we downloaded like this, then we will need to download the new version manually (overwriting existing files without regard).

### Clone with HTTPS

Cloning with HTTPS will let us use our git client to download a copy of the repository. Using the same client we can retrieve new updated versions of the repository and update files with the git version control system.

Through using your account username/password pair you are able to contribute your changes to a remote repository. While GitHub suggests this as the recommend means of connecting, we disagree and instead suggest the use of SSH keys.

### Clone with SSH

Much like the HTTP clone, the SSH version will work with a git client and will let us easily retrieve changes. However, it will further give us the ability to contribute directly to the same repository using our SSH credentials.

This will save a lot of time in the long run and is the preferred method that should be used when working with git. Through this method, the only devices that can modify your repositories are the ones that are manually set up by you, and they require unique passphrases to work.

**Problems with port 22**

If you encounter network related problem due to using port 22, you may add the following to the `config` file inside `~/.ssh/`:

```
Host gitlab.com
 Hostname altssh.gitlab.com
 User git
 Port 443
 PreferredAuthentications publickey
 IdentityFile ~/.ssh/id_ed25519
```

Remember to save the file, and you may need to restart you bash session.

## Remotes (Origin)

Remotes represent the different server hosted versions of your source code. These may be on physical computers you have set up or accounts with GitHub or GitLab, anything that is running the git server software.

> A guide to setting up your own server can be found in the [Git Server Documentation](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server).

When you have one of these _remote versions of a repository_ we call it a _remote_.

We label these with convenient names, the default of which is `origin`.
You may freely name remotes for your own convenience using your git client. And you may setup multiple remotes if needed, such ask keeping a copy of your source code in a secondary remote named `backup`.

## Branches

Branching is a vital part of collaboration. A branch is a different version of the same source code on the same server.

When we branch we create a new branch out of the previous one, this can be seen as a split. After the branch, we have the original version and the branched version. These can now be modified separately without affecting each other.

The default first version of your source code is called `main`.

You may freely rename `main` or other branches as you see fit, but it is advised to retain the `main` branch.

When describing a branch we can also denote it's location, for example:

**Example Branches:**

```
origin/main
backup/main

origin/dev
origin/release
```

When referring to local branches (on your own computer) you do not include the remote name.

**Local Branches:**

```
main
dev
release
```

### Creating and switching branches

We create branches by using the branch argument, for example to create the branch called `feature` we use the command:

```bash
$ git branch feature
```

We can the switch the new branch by using the `checkout` argument:

```bash
$ git checkout feature
```

It is possible to create a branch at the same time as checking it out:

```bash
$ git checkout -b another_feature
```

> It is also possible to checkout specific commits, but this will leave you detached from any branch and you should not normally do this.

### Branch Names

Branch names should indicate either ownership or purpose or both.

So if I were to create a bug fixing branch (locally), I would name it:

```
craigm-bugfix
```

The convention of `owner`-`purpose` is a simple one that avoids any confusion.

#### Remote Branches

As we said earlier, you will have a `main` branch (local and remote), but it's not typical to create many remote branches. Instead, remote branches should match version, milestones or tasks:

**Remote Branch Examples**

```
origin/main
origin/test
origin/experimental
origin/dev
origin/release
```

### The golden rule of branches

**Branch early, branch often.**

You may always create as many branches as you need, in practise it's best not to do this on remotes, but say to do on your local machine.

Remote branches will be visible to all contributors and may create unnecessary clutter for them.

## Commits

> A commit is a `save point` for your code.

When you make changes to your code, you will periodically want to save those changes. These changes are different from you saving a file within you IDE. Instead, this is a concerted choice to record progress that has been made.

You may re-write and save a piece of code many times before you are happy with how it works; all the time-saving file changes and build new versions of the software. Not until you are satisfied you have made progress on a task will you want to contribute the changes towards the final version of the project.

> It is also a good idea to get in the habit of making commits at regular intervals throughout the day, such as before lunch or when packing up at the end of the day.

Even if you make further changes, you may always revert them back to a commit.

> Commits are made to the local versions of the repository, not to _remotes_.

### Adding files to a commit

Before performing a commit you must stage the changes, you do this with the `add` argument:

```bash
$ git add filename.txt
```

It is also possible to add all files using one of the following (depending on circumstance):

Add all changes:
```bash
$ git add .
```

Add all changes that are not new files
```bash
$ git add -u
```

### Checking Status

At any time it is useful to check the status of files using the following:

```bash
$ git status
```

This will list the status of all files within the git repository, including list what changes have been staged.

### Creating a Commit

To finalize all staged changes we run the commit argument:

```bash
$ git commit
```

We will then be presented with a text editor (probably `Vim`), and will be asked to enter a commit message.

Alternatively we can use the following to avoid `Vim`:

```bash
$ git commit -m "A commit message"
```

### Commit Messages

Every commit you create must have a message that describes what the given commit has achieved.

Commit messages should always be two things:

- Meaningful
- Brief

A commit message may technically be multi-line, in practise only the first line ever gets looked at.

### Conventional Commits

Conventional Commits is a set of standards set out to help developers create better commit messages.

It is highly recommended that you follow the specification of [Conventional Commits](https://www.conventionalcommits.org) both in your personal projects and while working within teams.

There also have the added benefit that by using a commit message convention, you can create automated software changelogs for the project.

## Pulling & Pushing

These are the two methods that you will use to copy source code changes between the local repositories and the remote repositories.

Pulling is the act of fetching changes from the remote. This will (by default) pull for **all branches** that exist on the remote, this is why it is a good idea to local development branches that exist only on your local computer.

> You should always _pull_ before you _push_. You have better (easier) control to fix problems on your own local machine.

Pushing is the act of sending your committed changes to the remote repository, but if anyone has made changes that are not compatible with what you have done, then you will encounter problems[^1].

[^1]: The sheer volume and types of errors that can occur cannot be covered here. Instead, we suggest you follow a diligent set of practices to avoid conflict creating scenarios.

## Merging

Eventually, you will need to save changes that are made on one branch into a branch closer to `main` (or `main` itself).

This is done through a merge.

### Merging Locally

Simply tell you git client to merge one branch into another.

For example, I could merge `bugfix` into `dev`. By doing this I am asking my git to calculate all the difference between the two branches and try to "squash" one into the other. If no conflicting changes have occurred, then the client can automatically merge the one branch into the other.

If conflicts do occur you will need to manually edit files to complete the merge. You git client may or may not visually assist you with this process.

> You may optionally delete the _side_ branch after the merge, so only the final branch remains.

This kind of merge is perfectly fine for you to do when working only with your own source code, but more typically you will be in a team and changes may affect source code beyond your own.

### Merging Remotely (Preferred)

The better way to merge branches is to create a merge request from the GitHub/GitLab website.

We can select one branch and create a merge request into another. We will need to write a short description to explain the purpose of the merge and optionally we can assign someone to review it.

> Always try to get another developer to review the merge for you.

The best practice in this scenario is to leave the assignee open and instead the first available developer should handle the merge request. depending on the project this reviewer may need to be a senior developer or someone with elevated rights on the remote repository.

## History

One of the great aspects of git is that it preserves all history for each file that you commit.

We cna use the `log` argument to see this:

```bash
$ git log
```

This will list each commit from newest to oldest, top to bottom. Use the arrow keys (or `J`/`K`) to move up and down, `CTRL-Z` to exit.   

For a more visual representation we can add a few optional commands:
```bash
$ git log --graph --oneline --all
```

This will produce a full tree of all commits in the typical graph format you may expect, with each commit shown on only one line each.


### Visual Clients

There are many visual clients available that will help you perform all the task described here (and more), many IDEs even boast built-in git clients that can be very handy. Under the hood, all git clients will still perform the same git commands (via an internal command line).

My personal preference is to run commands manually, but it useful to visualize repositories with tools such as [Sublime Merge](https://www.sublimemerge.com/) or [Git Kraken](https://www.gitkraken.com/). It is important to remember that while the client will help you with tasks, you must still understand which git command needs to be executed.

For VSCode there is a plugin available called [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)

## The barebones of any Git project

Any project you create should always have the following two files:

- `README.md`
- `.gitignore`

### README.md

This file is really just a description of the project and its contributors, but you may add any information you desire here.

It is common practise to add build instructions here.

This file is written in markdown.

### .gitignore

There are some files (or folders) that you should not push to remote repositories, typically these are files such as compiled source code, libraries that will be downloaded by your build system, sensitive files, or system files.

Git has a built-in system to handle this problem, it is called "ignored files", which are tracked in the `.gitignore` file.

> The purpose here is to keep the remote repository clean and tidy, and to not track files that git cannot version control.

You may manually add to this file directly or thought git commands, but the easiest is to generate an encompassing `.gitignore` file by using the online tool at [gitignore.io](https://www.toptal.com/developers/gitignore).

[gitignore.io](https://www.toptal.com/developers/gitignore) requires that you list the operation systems, programming languages, build systems and IDEs that will affect your project; it will then generate the contents of a `.gitignore` file that you may save or manually copy across.

## Moving Backwards
> WARNING! THIS IS DANGEROUS!

It is possible to move a branch backwards by using the revert argument and specifying the commit number, in tis example we also keep the HEAD attached:

```bash
$ git revert --no-commit 0766c053..HEAD
$ git commit
```

## A Final Reminder

These are the two rules that will help you avoid _most_ problems that you will encounter when working with git:

- Branch early, branch often
- _pull_ before you _push_

## A footnote on converting old `master` branches into `main`

If you have old `master` branches that you want to switch over to the new standard of `main`, then please follow the guide below:

[Renaming your master branch](https://dev.to/horus_kol/renaming-your-master-branch-2a37)

---

Notes compiled by Craig Marais (@muskatel) & Greg Linklater (@EternalDeiwos)
