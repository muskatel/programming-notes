# Setting up ZSH

> You may need to install the fonts as described above.

Now that you have configured `Alacritty`, we need to configure the shell.

We start by running Alacritty and typing the following command:

```bash
$ sudo apt install zsh
```

This will istall `zsh` (pronounced "zish", rhymes with "fish"), and in future `zsh` will launch automatically, but we can start it now manually byt typing

```bash
$ zsh
```

We will be customizing `zsh` by using a framework called `oh my zsh` ([oh my zsh homepage](https://ohmyz.sh/)).

We can use `curl` or `wget` to download `oh my zsh` for us:

**curl**
```bash
$ sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

**wget**
```bash
$ sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

By default `oh my zsh` includes a great selection of themes and configurations for us to use, examples of these are visible on the github link: [Themes for oh my zsh](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes).

Finally, we could install the `Power Level 10k` theme which is what I use personally.

## Power Level 10k

Start by cloning the theme directly into the `zsh` custom themes folder:

```bash
$ git clone https://github.com/romkatv/powerlevel10k.git $ZSH_CUSTOM/themes/powerlevel10k
```

Edit the `~/.zshrc` file to use the `Power Level 10k` theme:

**Using nano:**
```
$ nano ~/.zshrc
```

**Using vim:**
```
$ vim ~/.zshrc
```

Find the line the specifies `ZSH_THEME` and change it to the following:

```
ZSH_THEME="powerlevel10k/powerlevel10k"
```

Then save and close the file.

Once you have saved the theme you can restart `zsh` by simply running zsh:

```bash
$ zsh
```

If `Power Level 10k` is setup correctly it will start the on screen configuration tool.

You can re-run this configuration again by using the following command:

```bash
$ p10k configure
```

## Customizing zsh

Start by opening the `~/.zshrc` file for editing:

**Using nano:**

```
$ nano ~/.zshrc
```

**Using vim:**

```
$ vim ~/.zshrc
```

At the very bottom you can add aliases (shortcuts) as following:

```
# VSCode
# On my older computer I needed to add this,
# but newer install probably wont need it
alias code='/mnt/c/Users/<your username>/AppData/Local/Programs/Microsoft\ VS\ Code/bin/code
```

This allows launching VSCode directly from the shell,

```bash
$ code
```

opening a folder,

```bash
# Open here
$ code .

# Open C:\Users (from WSL)
$ code /mnt/c/Users/
```

or using VSCode to edit files:

```bash
$ code ~/.zshrc
```

---

Notes compiled by Craig Marais (@muskatel)
